require 'spec_helper'

RSpec.describe Chiligum::Dam::Assets, type: :request do
  before do
    allow_any_instance_of(Chiligum::Dam::Assets).to receive(:upload_by_url).with(String, String)
                                                                           .and_return(response_api)
  end

  let(:creative_params) { { 'x-api-key': SecureRandom.hex(8) } }
  let(:folder_path) { SecureRandom.hex(8) }

  describe '#upload_by_url' do
    subject { JSON.parse Chiligum::Dam::Assets.new(creative_params).upload_by_url(file_url, folder_path) }

    context 'when upload is successfull' do
      let(:filename) { SecureRandom.hex(8) }
      let(:file_extension) { 'jpg' }
      let(:file_url) { Faker::Internet.url(host: 'example.com', path: "/#{filename}.#{file_extension}") }

      let(:response_api) do
        FactoryBot.build(:upload_by_url, filename: filename, extension: file_extension).marshal_dump.to_json
      end

      it { expect(subject.dig('data', 'type')).to eq 'asset' }
      it { expect(subject.dig('data', 'attributes', 'name')).to eq filename }
      it { expect(subject.dig('data', 'attributes', 'file_extension')).to eq file_extension }
    end

    context 'when post is unsuccessfull' do
      context 'when url is invalid' do
        let(:file_url) { Faker::Internet.url }

        let(:response_api) do
          FactoryBot.build(:upload_by_url_invalid_url).marshal_dump.to_json
        end

        it { expect(subject['error']).to eq 'The url is invalid' }
      end

      context 'when file extension is invalid' do
        let(:file_url) { Faker::Internet.url(path: "#{SecureRandom.hex(8)}.html") }

        let(:response_api) do
          FactoryBot.build(:upload_by_url_invalid_extension).marshal_dump.to_json
        end

        it { expect(subject['error']).to eq 'Does not support this file extension, only [\"png\", \"jpg\", \"jpeg\"]' }
      end
    end
  end
end
