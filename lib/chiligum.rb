require 'rest-client'

require 'chiligum/version'
require 'chiligum/base'

# Automation
require 'chiligum/automation/base'
require 'chiligum/automation/creative_jobs'

# dam
require 'chiligum/dam/base'
require 'chiligum/dam/assets'
