FactoryBot.define do
  factory :job_collector, class: OpenStruct do
    data do
      {
        id: job_identifier,
        type: 'creative_job',
        attributes: {
          identifier: SecureRandom.hex(8),
          template: {
            id: SecureRandom.hex(8),
            type: 'template',
            creative_type: 'image'
          },
          creative_job_items: [
            {
              id: SecureRandom.hex(8),
              type: 'creative_job_item',
              attributes: {
                asset: {
                  identifier: SecureRandom.hex(8),
                  public_url: Faker::Internet.url(host: 'chiligum-platformv2-assets.s3.amazonaws.com', path: "#{SecureRandom.hex(8)}/#{SecureRandom.hex(8)}/#{SecureRandom.hex(8)}")
                }
              }
            }
          ]
        }
      }
    end

    trait :unfinished do
      data do
        {
          id: job_identifier,
          type: 'creative_job',
          attributes: {
            identifier: SecureRandom.hex(8),
            template: {
              id: SecureRandom.hex(8),
              type: 'template',
              creative_type: 'image'
            },
            creative_job_items: []
          }
        }
      end
    end

    meta do
      {
        progress_percentage: job_progress
      }
    end
  end
end
