# nodoc
module Chiligum
  # nodoc
  module Dam
    # nodoc
    class Base < Chiligum::Base
      def base_endpoint
        "#{base_url}/dam"
      end
    end
  end
end
