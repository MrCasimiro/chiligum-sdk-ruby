FactoryBot.define do
  factory :template_feed, class: OpenStruct do
    data do
      [
        {
          id: 'cad8a96e-2bf4-45da-9c15-e98994c70506',
          type: 'layer',
          attributes: {
            name: '5.BACKGROUND',
            identifier: 'cad8a96e-2bf4-45da-9c15-e98994c70506',
            type: 'shape',
            variable_name: 'shape_variable',
            initial_value: '#5c18a8',
            required: false
          }
        },
        {
          id: '8fc57623-1311-47ac-a74d-6ba3f91593e8',
          type: 'layer',
          attributes: {
            name: '3.PERSONAGEM',
            identifier: '8fc57623-1311-47ac-a74d-6ba3f91593e8',
            type: 'image',
            variable_name: 'image_variable',
            required: false,
            extension: nil
          }
        },
        {
          id: '9cbb8a3d-0926-4c35-a4e6-dee6121d97a1',
          type: 'layer',
          attributes: {
            name: '2.DESCONTO',
            identifier: '9cbb8a3d-0926-4c35-a4e6-dee6121d97a1',
            type: 'text',
            variable_name: 'text_variable',
            text_editable: true,
            color_editable: false,
            initial_value_text_color: '#343434',
            characters: {
              max: '0',
              min: '0'
            },
            required: false
          }
        }
      ]
    end
  end

  factory :template_not_found, class: OpenStruct do
    error { "Couldn't find ChiliRecordDam::Model::Template" }
  end
end
