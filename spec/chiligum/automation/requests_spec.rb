require 'spec_helper'

RSpec.describe Chiligum::Automation::Requests, type: :request do
  before do
    allow_any_instance_of(Chiligum::Automation::Requests).to receive(:fetch_feed).with(String)
                                                                                 .and_return(response_api)
  end

  let(:creative_params) { { 'x-api-key': SecureRandom.hex(8) } }
  let(:template_identifier) { SecureRandom.hex(8) }

  describe '#fetch_feed' do
    subject { JSON.parse Chiligum::Automation::Base.new(creative_params).fetch_feed(template_identifier) }
    context 'when template is found' do
      let(:response_api) do
        FactoryBot.build(:template_feed).marshal_dump.to_json
      end

      it { expect(subject['data'].length).to be > 0 }
      it { expect(subject['data'][0]['type']).to eq 'layer' }
    end

    context 'when template is not found' do
      let(:response_api) do
        FactoryBot.build(:template_not_found).marshal_dump.to_json
      end

      it { expect(subject['error']).to eq "Couldn't find ChiliRecordDam::Model::Template" }
    end
  end

  describe '#fetch_job_progress' do
    subject do
      JSON.parse Chiligum::Automation::CreativeJobs.new(creative_params).fetch_job_progress(job_identifier)
    end

    let(:job_identifier) { SecureRandom.hex(8) }

    before do
      allow_any_instance_of(Chiligum::Automation::Requests).to receive(:fetch_job_progress).with(String)
                                                                                               .and_return(response_api)
    end

    context 'with creative_type image finished' do
      let(:response_api) do
        FactoryBot.build(:job_collector, job_identifier: job_identifier, job_progress: job_progress)
                  .marshal_dump.to_json
      end

      let(:job_progress) { 100 }

      it { expect(subject.dig('data', 'id')).to eq job_identifier }
      it { expect(subject.dig('data', 'attributes', 'template', 'id')).to be_truthy }
      it { expect(subject.dig('data', 'attributes', 'creative_job_items')[0]['id']).to be_truthy }
      it { expect(subject.dig('data', 'attributes', 'creative_job_items')[0].dig('attributes', 'asset')).to be_truthy }
    end

    context 'with creative_type image unfinished' do
      let(:response_api) do
        FactoryBot.build(:job_collector, :unfinished, job_identifier: job_identifier, job_progress: job_progress)
                  .marshal_dump.to_json
      end

      let(:job_progress) { 0 }

      it { expect(subject.dig('data', 'id')).to eq job_identifier }

      it { expect(subject.dig('data', 'attributes', 'template', 'id')).to be_truthy }
    end
  end

  describe '#create_job' do
    subject { JSON.parse Chiligum::Automation::CreativeJobs.new(creative_params).create_job(payload) }
    before do
      allow_any_instance_of(Chiligum::Automation::Requests).to receive(:create_job)
        .with({ template_uuid: String, extensions: Array, items: Hash })
        .and_return(response_api)
    end

    let(:payload) do
      {
        template_uuid: SecureRandom.hex(8),
        extensions: ['png'],
        items: items
      }
    end

    context 'when post is successfull' do
      context 'without folder_path' do
        let(:items) do
          {
            '1': [
              {
                identifier: SecureRandom.hex(8),
                asset_identifier: SecureRandom.hex(8)
              }
            ]
          }
        end

        let(:response_api) { FactoryBot.build(:create_job).marshal_dump.to_json }
        it { expect(subject.dig('data', 'id')).to be_present }
        it { expect(subject.dig('data', 'type')).to eq 'creative_job' }
      end

      context 'with folder_path' do
        pending
      end
    end

    context 'when post is unsuccessfull' do
      let(:items) do
        {
          '1': [
            {
              identifier: SecureRandom.hex(8),
              asset_identifier: SecureRandom.hex(8)
            }
          ]
        }
      end

      context 'with asset not found' do
        let(:response_api) { FactoryBot.build(:create_job_asset_not_found).marshal_dump.to_json }
        it { expect(subject['error']).to eq 'Asset object not found, double check your request' }
      end

      context 'with template not found' do
        let(:response_api) { FactoryBot.build(:create_job_template_not_found).marshal_dump.to_json }
        it { expect(subject['error']).to eq 'CreativeAutomationApi::TemplateNotFound' }
      end
    end
  end
end
