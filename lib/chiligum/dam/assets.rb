# nodoc
module Chiligum
  # nodoc
  module Dam
    # nodoc
    class Assets < Chiligum::Dam::Base
      def upload_by_url(file_url, folder_path)
        RestClient::Request.execute(method: :post, payload: mount_payload(file_url, folder_path),
                                    url: upload_by_url, headers: mount_header)
      end

      private

      def mount_payload(file_url, folder_path)
        { url: file_url, folder_identifier: folder_path }
      end

      def upload_by_url_endpoint
        "#{base_endpoint}/assets/upload/by-url"
      end
    end
  end
end
