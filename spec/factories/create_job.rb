FactoryBot.define do
  factory :create_job, class: OpenStruct do
    data do
      {
        id: SecureRandom.hex(8),
        type: 'creative_job',
        attributes: {
          identifier: SecureRandom.hex(8),
          status: 'pending'
        }
      }
    end
  end

  factory :create_job_unauthorized, class: OpenStruct do
    message { 'Unauthorized' }
  end

  factory :create_job_asset_not_found, class: OpenStruct do
    error { 'Asset object not found, double check your request' }
  end

  factory :create_job_template_not_found, class: OpenStruct do
    error { 'CreativeAutomationApi::TemplateNotFound' }
  end
end
