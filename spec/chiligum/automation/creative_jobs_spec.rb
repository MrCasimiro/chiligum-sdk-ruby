require 'spec_helper'

RSpec.describe Chiligum::Automation::CreativeJobs, type: :request do
  let(:creative_params) { { 'x-api-key': SecureRandom.hex(8) } }

  describe '#fetch_variables' do
    subject do
      Chiligum::Automation::CreativeJobs.new(creative_params).fetch_variables(template_identifier)
    end

    let(:template_identifier) { SecureRandom.uuid }

    before do
      allow_any_instance_of(Chiligum::Automation::Requests).to receive(:fetch_feed).with(String)
                                                                                       .and_return(response_api)
    end

    let(:response_api) do
      FactoryBot.build(:template_feed).marshal_dump.to_json
    end

    let(:variables) do
      [{ 'name' => 'shape_variable', 'type' => 'shape' },
       { 'name' => 'image_variable', 'type' => 'image' },
       { 'name' => 'text_variable', 'type' => 'text' }]
    end

    it { expect(subject.map { |v| v['name'] }).to eq(variables.map { |v| v['name'] }) }
  end

  describe '#mount_items' do
    subject do
      Chiligum::Automation::CreativeJobs.new(creative_params).mount_items(template_identifier, items)
    end

    let(:template_identifier) { SecureRandom.uuid }

    before do
      allow_any_instance_of(Chiligum::Automation::Requests).to receive(:fetch_feed).with(String)
                                                                                       .and_return(response_api)
    end

    let(:response_api) do
      FactoryBot.build(:template_feed).marshal_dump.to_json
    end

    context 'when items does not have filename' do
      let(:items) do
        [{ 'shape_variable' => Faker::Color.hex_color, 'image_variable' => SecureRandom.uuid,
           'text_variable' => Faker::String.random }]
      end

      it do
        expect(subject['1']).to include({ 'identifier' => String, 'shape_color' => String },
                                        { 'identifier' => String, 'asset_identifier' => String },
                                        { 'identifier' => String, 'caption' => String })
      end
    end

    context 'when items does have filename' do
      let(:filename) { Faker::Alphanumeric.alpha(number: 10) }
      let(:items) do
        [{ 'shape_variable' => Faker::Color.hex_color, 'image_variable' => SecureRandom.uuid,
           'text_variable' => Faker::String.random, '__filename' => filename }]
      end

      it do
        expect(subject["1_#{filename}"]).to include({ 'identifier' => String, 'shape_color' => String },
                                                    { 'identifier' => String, 'asset_identifier' => String },
                                                    { 'identifier' => String, 'caption' => String })
      end
    end
  end
end
