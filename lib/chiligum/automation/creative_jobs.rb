# nodoc
module Chiligum
  # nodoc
  module Automation
    # nodoc
    class CreativeJobs < Chiligum::Automation::Base
      def fetch_variables(template_identifer)
        response = JSON.parse(fetch_feed(template_identifer))
        variables = []
        response['data'].map do |layer|
          variable = {}
          variable['id'] = layer['id']
          variable['name'] = layer.dig('attributes', 'variable_name')
          variable['type'] = layer.dig('attributes', 'type')
          variables << variable
        end

        variables
      end

      def mount_create_job_payload(template_uuid, extensions, items, folder_path = nil)
        payload = {
          template_uuid: template_uuid,
          extensions: extensions,
          items: mount_items(template_uuid, items)
        }

        payload[:folder_path] = folder_path if folder_path

        payload
      end

      def mount_items(template_identifer, items)
        variables = fetch_variables(template_identifer)
        items_payload = {}
        items.map.with_index do |item, i|
          item_key = (i + 1).to_s
          item_key += "_#{item['__filename']}" unless item['__filename'].to_s.empty?
          items_payload[item_key] = []
          variables.map do |variable|
            value = item[variable['name']]
            items_payload[item_key] << __send__("mount_#{variable['type']}_variable", variable['id'], value)
          end
        end

        items_payload
      end

      private

      def mount_image_variable(id, asset_identifier)
        {
          'identifier' => id,
          'asset_identifier' => asset_identifier
        }
      end

      def mount_text_variable(id, value)
        {
          'identifier' => id,
          'caption' => value
        }
      end

      def mount_shape_variable(id, color)
        {
          'identifier' => id,
          'shape_color' => color
        }
      end
    end
  end
end
