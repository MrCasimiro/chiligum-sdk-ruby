require 'rest-client'

# nodoc
module Chiligum
  # nodoc
  module Automation
    # nodoc
    module Requests
      def fetch_feed(template_identifer)
        ::RestClient::Request.execute(method: :get, url: feed_endpoint(template_identifer), headers: mount_header)
      end

      def fetch_job_progress(job_identifier)
        RestClient::Request.execute(method: :get, url: job_progress_endpoint(job_identifier), headers: mount_header)
      end

      def create_job(template_uuid:, extensions:, items:, folder_path: nil)
        payload = mount_create_job_payload(template_uuid, extensions, items, folder_path)
        RestClient::Request.execute(method: :post, url: creative_jobs_endpoint, payload: payload, headers: mount_header)
      end

      private

      def base_endpoint
        "#{base_url}/automation"
      end

      def job_progress_endpoint(job_identifier)
        base_endpoint + "/creative-jobs/#{job_identifier}/job-progress"
      end

      def creative_jobs_endpoint
        "#{base_endpoint}/creative-jobs"
      end

      def feed_endpoint(template_identifer)
        base_endpoint + "/templates/#{template_identifer}/feed"
      end
    end
  end
end
