FactoryBot.define do
  factory :upload_by_url, class: OpenStruct do
    data do
      {
        id: SecureRandom.hex(8),
        type: 'asset',
        attributes: {
          identifier: SecureRandom.hex(8),
          name: filename,
          file_extension: extension,
          mime_type: nil,
          mime_subtype: nil,
          preview_url: nil,
          updated_at: '2021-02-25T18:27:03.722Z',
          created_at: '2021-02-25T18:27:03.722Z',
          updated_by: {
            identifier: SecureRandom.hex(8),
            name: 'John Doe'
          },
          details: {
            size: 224_446
          },
          tags: {
            data: []
          }
        }
      }
    end
  end

  factory :upload_by_url_invalid_url, class: OpenStruct do
    error { 'The url is invalid' }
  end

  factory :upload_by_url_invalid_extension, class: OpenStruct do
    error { 'Does not support this file extension, only [\"png\", \"jpg\", \"jpeg\"]' }
  end
end
