# nodoc
module Chiligum
  # nodoc
  class Base
    attr_accessor :api_key

    def initialize(x_api_key)
      @api_key = x_api_key
    end

    private

    def mount_header
      { 'x-api-key' => @api_key }
    end

    def base_url
      'https://kypcnygbsk.execute-api.sa-east-1.amazonaws.com/staging/api'
    end
  end
end
