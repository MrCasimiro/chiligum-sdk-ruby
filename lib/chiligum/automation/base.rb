require_relative 'requests'

# nodoc
module Chiligum
  # nodoc
  module Automation
    # nodoc
    class Base < Chiligum::Base
      include Automation::Requests
    end
  end
end
